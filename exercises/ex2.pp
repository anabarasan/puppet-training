## ex2.pp

group { 'nokia-admin':
    ensure  =>  'present'
}

user { 'dev' :
    ensure      =>  'present',
    password    =>  'user1',
    groups      =>  'nokia-admin'
}

host { 'master.com': 
    ensure  =>  'present',
    ip      =>  '13.127.137.108'
    host_aliases    =>  'master.com'
}
