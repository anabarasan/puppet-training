## ex1.pp

package { 'httpd': 
    ensure  =>  'present'
}

file { '/var/www/html/index.html': 
    ensure  =>  'present',
    content =>  "<html><body><h1>Home Page content from variable</h1></body></html>"
}

service { 'httpd':
    ensure  =>  'running'
}

notify {"httpd installed and home page configured":}

