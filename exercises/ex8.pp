## ex8.pp
## conditionals using facter

if $facts['os']['family'] == 'RedHat' {
    package { 'httpd':
        ensure => 'present'
    }
    notify {"httpd installed":}
}

if $facts['os']['family'] == 'Ubuntu' {
    notify {"Nothing to do":}
}

else {
    notify {"Debian":}
}

