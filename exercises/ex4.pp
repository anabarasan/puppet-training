## ex4.pp

$home_page = "<html><body><h1>Home Page content from variable</h1></body></html>"

package { 'httpd': 
    ensure  =>  'present'
}

file { '/var/www/html/index.html': 
    ensure  =>  'present',
    content =>  $home_page
}

service { 'httpd':
    ensure  =>  'running'
}

notify {"httpd installed and home page configured":}
