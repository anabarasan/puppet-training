# Class: jenkins
# ===========================
#
# Full description of class jenkins here.
#
# Parameters
# ----------
#
# Document parameters here.
#
# * `sample parameter`
# Explanation of what this parameter affects and what it defaults to.
# e.g. "Specify one or more upstream ntp servers as an array."
#
# Variables
# ----------
#
# Here you should define a list of variables that this module would require.
#
# * `sample variable`
#  Explanation of how this variable affects the function of this class and if
#  it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#  External Node Classifier as a comma separated list of hostnames." (Note,
#  global variables should be avoided in favor of class parameters as
#  of Puppet 2.6.)
#
# Examples
# --------
#
# @example
#    class { 'jenkins':
#      servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#    }
#
# Authors
# -------
#
# Author Name <author@domain.com>
#
# Copyright
# ---------
#
# Copyright 2018 Your name here, unless otherwise noted.
#
class jenkins {

	$java_download = 'wget -c --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/jdk-8u131-linux-x64.rpm'

	$install_java	= "rpm -ivh jdk-8u131-linux-x64.rpm"

	$add_jenkins_repo	= "sudo wget -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat-stable/jenkins.repo"

	$import_jenkins_key	= "sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key"

	package { 'wget':
		ensure	=>	'present'
	}

	exec { 'download java':
		command	=> $java_download,
		cwd 	=> '/root',
		path	=> '/bin',
		creates	=> '/root/jdk-8u131-linux-x64.rpm',
	}

	exec { 'install java':
		command	=> $install_java,
		cwd	=> '/root',
		path	=> '/bin',
	}
	
	yumrepo { 'jenkins.repo': 
		ensure	=> 'absent',
		baseurl	=> 'https://pkg.jenkins.io/redhat-stable/jenkins.repo',
		enabled	=> true,
		gpgkey	=> 'https://pkg.jenkins.io/redhat-stable/jenkins.io.key'
	}

	exec { 'add jenkins repo':
		command	=> $add_jenkins_repo,
		cwd	=> '/root',
		path	=> '/bin',
	}

	exec { 'import jenkins key':
		command	=> $import_jenkins_key,
		cwd	=> '/root',
		path	=> '/bin',
	}

	package { 'jenkins':
		ensure	=> 'present'
	}

	service { 'jenkins':
		ensure	=> 'running',
		enable	=> true
	}
}
